from django.contrib import admin
from .models import Author,Record,Tag,Gender
# Register your models here.
admin.site.register(Author)
admin.site.register(Record)
admin.site.register(Tag)
admin.site.register(Gender)
