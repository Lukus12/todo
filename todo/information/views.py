from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import *
from .serializers import *
from rest_framework import viewsets
from django.shortcuts import get_object_or_404
# Create your views here.


class AuthorView(APIView):
    def get(self,request):
        authors = Author.objects.all()
        serializer = AuthorSerializer(authors, many=True)
        return Response({"authors": serializer.data})

    def post(self,request):
        author = request.data.get('authors')
        serializer = AuthorSerializer(data=author)
        if serializer.is_valid(raise_exception = True):
            authors_saved =  serializer.save()
        return Response({'ok':"vse ok"})

class AuthorViewSet(viewsets.ModelViewSet):
    serializer_class = AuthorSerializer
    queryset = Author.objects.all()
