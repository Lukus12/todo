from rest_framework.routers import DefaultRouter
from django.urls import path
from .views import AuthorViewSet

router = DefaultRouter()
router.register(r'information', AuthorViewSet, basename='info')

urlpatterns = router.urls
