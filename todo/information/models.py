from django.db import models
from django.utils import timezone
# Create your models here.
class Author(models.Model):
    title = models.CharField(
        verbose_name='Автор',
        max_length=100
    )
    description = models.TextField()
    gender = models.ForeignKey('Gender', on_delete = models.CASCADE)
    record = models.ForeignKey('Record', on_delete = models.CASCADE)
    created_date = models.DateTimeField(
        default=timezone.now,
        null=True,
        blank=True,
    )
    def _str_(self):
        return self.title

class Record(models.Model):
    records = models.CharField(
        verbose_name='Запись',
        max_length=100
    )
    tags = models.ForeignKey('Tag', on_delete = models.CASCADE)
    text = models.TextField()
    created_date = models.DateTimeField(
        default=timezone.now,
        null=True,
        blank=True,
    )
    def _str_(self):
        return self.records

class Tag(models.Model):
    tg = models.CharField(
        verbose_name='Теги записи',
        max_length=20
    )
    def _str_(self):
        return self.tg

class Gender(models.Model):
    pol=models.CharField(
        verbose_name='Пол',
        max_length=100
    )
    def _str_(self):
        return self.pol
